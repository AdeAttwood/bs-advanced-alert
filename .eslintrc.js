const isNode    = false;
const isBrowser = true;
const isEs6     = true;

// ./node_modules/eslint/bin/eslint.js -f unix -c .eslintrc.js src

module.exports = {
    'env': {
        'browser': isBrowser,
        'meteor': true,
        'node': isNode,
        'es6': isEs6
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'sourceType': 'module'
    },
    'rules': {
        'indent': [2, 4],
        'linebreak-style': [2, 'unix'],
        'quotes': [2, 'single'],
        'semi': [2, 'always'],
        'brace-style': [2, '1tbs'],
        'array-bracket-spacing': [2, 'never'],
        'camelcase': [2, {'properties': 'always'}],
        'keyword-spacing': [2],
        'eol-last': [2],
        'no-trailing-spaces': [2],
        "no-else-return" : 1,
        'no-console': [isBrowser ? 1 : 0],
        'valid-jsdoc': [1],
        'no-extra-parens': [1],
        'space-in-parens': ["error", "never"],
        'yoda': ["error", "never"]
    }
};