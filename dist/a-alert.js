'use strict';

/* global jQuery */

(function ($) {

    $.fn.aalert = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }

        $.error('Method ' + method + ' does not exist');
        return false;
    };

    var methods = {
        changeTo: function changeTo(valiant) {
            var $this = $(this);

            $this.removeClass(function (index, css) {
                return (css.match(/(^|\s)alert-\S+/g) || []).join(' ');
            });

            $this.addClass('alert-' + valiant);

            return this;
        },
        changeContentTo: function changeContentTo(content) {
            var open = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            var $this = $(this);

            $this.find('.alert-content').html(content);

            if (open === true) {
                methods.open.apply(this);
            }

            return this;
        },
        toggle: function toggle() {
            var $this = $(this);
            if ($this.hasClass('fade')) {
                $this.fadeToggle();
            } else if ($this.hasClass('slide')) {
                $this.slideToggle();
            } else {
                $this.toggle();
            }

            return this;
        },
        close: function close() {
            var $this = $(this);
            if ($this.hasClass('fade')) {
                $this.fadeOut();
            } else if ($this.hasClass('slide')) {
                $this.slideUp();
            } else {
                $this.hide();
            }

            if ($this.hasClass('remove')) {
                $this.remove();
            }

            return this;
        },
        open: function open() {
            var $this = $(this);
            if ($this.hasClass('fade')) {
                $this.fadeIn();
            } else if ($this.hasClass('slide')) {
                $this.slideDown();
            } else {
                $this.show();
            }

            return this;
        },
        closeAfter: function closeAfter(timeOut) {
            var that = this;
            setTimeout(function () {
                methods.close.apply(that);
            }, timeOut);

            return this;
        }
    };

    $('.alert [data-dismiss="aalert"]').click(function (e) {
        var $element = $(e.target).closest('.alert');
        methods.close.apply($element);
    });

    $('.alert[data-timeout]').each(function (index, element) {
        methods.closeAfter.apply(element, [$(element).data('timeout')]);
    });
})(jQuery);
//# sourceMappingURL=a-alert.js.map
