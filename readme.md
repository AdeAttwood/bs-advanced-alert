# Bootstrap Advanced Alert

## Basic usage

Working in the same way as the standard alert component.

~~~html
<div class="alert alert-warning in">
    <a href="#" class="close" data-dismiss="aalert">&times;</a>
    <div class="alert-content">
        <strong>Warning!</strong> There was a problem.
    </div>
</div>
~~~

## Adding open and close effects.

You can add open and close effects the same as the standard component. The available effects are `slide` and `fade`, classes can be added to the alert component to apply the effects. If no class is added the alert will just hide.

~~~html
<div class="alert alert-success slide in">
    <a href="#" class="close" data-dismiss="aalert">&times;</a>
    <div class="alert-content">
        <strong>Success!</strong> Well done.
    </div>
</div>

<div class="alert alert-info fade in">
    <a href="#" class="close" data-dismiss="aalert">&times;</a>
    <div class="alert-content">
        <strong>Info!</strong> This is some info.
    </div>
</div>
~~~

## Timeout

You can add a `data-timeout` attribute to close the alert after a set amount of time set in milliseconds.

~~~html
<div class="alert alert-info fade in" data-timeout="1000">
    <a href="#" class="close" data-dismiss="aalert">&times;</a>
    <div class="alert-content">
        <strong>Info!</strong> This is some info.
    </div>
</div>
~~~

The above sample will close after 1 second

## jQuery extension api

### `changeTo`

This will change the alert style. The below snippet with change the `alert-` class to `alert-error`.

~~~js
$('my-alert').aalert('changeTo', 'error');
~~~

### `changeContentTo`

The will change the content of the alert. The function takes a optional second parameter `open`. This will open the alert after the content has been changed and is `true` by default.

~~~js
$('.my-alert').aalert('changeContentTo', 'Some new content', false);
~~~

### `closeAfter`

This will close the alert after the specified time

~~~js
$('my-alert').aalert('closeAfter', 2000);
~~~

### `toggle`

~~~js
$('my-alert').aalert('toggle');
~~~

### `close`

~~~js
$('my-alert').aalert('close');
~~~

### `open`

~~~js
$('my-alert').aalert('open');
~~~

## Changing functions

~~~js
$('.my-alert')
    .aalert('changeTo', 'success')
    .aalert('changeContentTo', 'Success message')
    .aalert('closeAfter', 3000);
~~~