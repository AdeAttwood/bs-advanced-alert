module.exports = grunt => {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        babel: {
            options: {
                sourceMap: true,
                presets: ['env']
            },
            dist: {
                files: {
                    'dist/a-alert.js': './index.js'
                }
            }
        },
        uglify: {
            dist: {
                options: {
                    mangle: true,
                    output: {
                        comments: false
                    }
                },
                files: {
                    'dist/a-alert.min.js': ['dist/a-alert.js']
                }
            }
        },
        eslint: {
            target: ['index.js', 'dist/a-alert.js']
        }
    });

    grunt.registerTask('default', ['babel', 'uglify', 'eslint']);
};
