/* global jQuery */

(($) => {

    $.fn.aalert = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }

        $.error('Method ' + method + ' does not exist');
        return false;
    };

    var methods = {

        changeTo(valiant) {
            var $this = $(this);

            $this.removeClass((index, css) => {
                return (css.match (/(^|\s)alert-\S+/g) || []).join(' ');
            });

            $this.addClass('alert-' + valiant);

            return this;
        },

        changeContentTo(content, open = true) {
            var $this = $(this);

            $this.find('.alert-content').html(content);

            if (open === true) {
                methods.open.apply(this);
            }

            return this;
        },

        toggle() {
            var $this = $(this);
            if ($this.hasClass('fade')) {
                $this.fadeToggle();
            } else if ($this.hasClass('slide')) {
                $this.slideToggle();
            } else {
                $this.toggle();
            }

            return this;
        },

        close() {
            var $this = $(this);
            if ($this.hasClass('fade')) {
                $this.fadeOut();
            } else if ($this.hasClass('slide')) {
                $this.slideUp();
            }  else {
                $this.hide();
            }

            if ($this.hasClass('remove')) {
                $this.remove();
            }

            return this;
        },

        open() {
            var $this = $(this);
            if ($this.hasClass('fade')) {
                $this.fadeIn();
            } else if ($this.hasClass('slide')) {
                $this.slideDown();
            } else {
                $this.show();
            }

            return this;
        },

        closeAfter(timeOut) {
            var that = this;
            setTimeout(() => {
                methods.close.apply(that);
            }, timeOut);

            return this;
        }

    };

    $('.alert [data-dismiss="aalert"]').click(e => {
        var $element = $(e.target).closest('.alert');
        methods.close.apply($element);
    });

    $('.alert[data-timeout]').each((index, element) => {
        methods.closeAfter.apply(element, [$(element).data('timeout')]);
    });

})(jQuery);
